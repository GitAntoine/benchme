# BenchMe


TP COLLE Développement 

## Pour commencer

Les algorithmes de tri permettent de trier les tableaux. <br>
Nous allons en étudier trois sortes :<br> 
- Tri a bulle <br>
- Tri par sélection <br>
- Tri par insértion <br>

### Pré-requis

Pour pouvoir éxecuter ces algorithmes, il faut

- Un IDE (De préfèrence NetBeans pour éviter des problèmes de compatibilité)

### Installation

Il faudra dans un premier temps créer un nouveau projet intitulé BenchMe sur NetBeans

Ensuite il faudra créer dans Header Files un nouveau fichier C Header File nommé Header <br>
Ensuite on créer un fichier C Source File dans Source File nommé function.

Il faut ensuite copier coller le contenu de notre projet dans les fichiers que l'on vient de créer qui ont le même nom




## Démarrage et résutats attendu 

Pour lancer les algorithmes il suffit d'appuyer sur la touche F6.
On devrait alors avoir notre tableau trié par ordre croissant

## Fabriqué avec

Entrez les programmes/logiciels/ressources que vous avez utilisé pour développer votre projet


* [NetBeans](https://netbeans.apache.org/download/index.html) - IDE




## Versions


**Dernière version stable :** 1.0
**Dernière version :** 1.0
Liste des versions : [Cliquer pour afficher](https://gitlab.com/GitAntoine/benchme/tags)

## Auteurs
Listez le(s) auteur(s) du projet ici !
* **Lucas Chanaux** _alias_ [@luc614fr](https://github.com/luc614fr)
* **Antoine Chavée** _alias_ [@GitAntoine](https://github.com/GitAntoine)






